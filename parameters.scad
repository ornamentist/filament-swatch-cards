//
// Filament swatch box design parameters.
//
// Since the only data structure OpenSCAD supports is vectors, each
// design sub-part is parameterized as a vector of sub-part-specific
// values.
//
// Unfortunately this approach leads to heavy use of magic-number
// indexes in client code. So far this seems preferable to having
// functions with large parameter lists or referencing a large number of
// global variables.
//

//
// Filament swatch card geometry (in mm).
//
CARD_BODY = [
  4.0,              // Card depth.
  4.0,              // Corner rounding.
  0.9,              // Edge rounding.
  [84.0, 53.4, 4.0] // Card spans (dx / dy / dz).
];


//
// Filament swatch card notch geometry (in mm).
//
CARD_NOTCH = [
  3.0,                 // Notch radius.
  5.0,                 // Notch depth.
  0.6,                 // Edge rounding.
  [[17.0, 53.5, -0.5]] // Notch origins.
];


//
// Filament swatch card inset geometry (in mm).
//
CARD_INSET = [
  2.5,              // Inset depth.
  2.1,              // Corner rounding.
  0.6,              // Edge rounding.
  [33.6, 23.36],    // Inset spans.
  [44.0, 5.34, 2.0] // Inset origin.
];


//
// Filament swatch card details geometry (in mm).
//
CARD_TEXT = [
  2.8,              // Depth.
  [5.0, 40.0, 2.0]  // Origin.
];


//
// Filament swatch box body geometry.
//
BOX_BODY = [
  20,         // Box capacity in cards.
  3.0,        // Box wall thickness (X/Y).
  3.0,        // Box corner radius (X/Y).
  0.7,        // Box top edge radius.
  1.8,        // Box divider thickness.
  2.0,        // Box base thickness.
  0.45,       // Box height scale.
  [1.1, 0.90] // Box slot padding.
];


//
// Normalized X/Y/Z axes as convenient vectors.
//
X_AXIS = [1, 0, 0];
Y_AXIS = [0, 1, 0];
Z_AXIS = [0, 0, 1];

