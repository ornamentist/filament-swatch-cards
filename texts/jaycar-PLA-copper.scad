//
// Filament swatch card template expansion:
//
// Filament: Jaycar PLA
// Color: "Copper".
//


//
// Arial rounded fonts seem to print legibly.
//
FONT_ARIAL = "Arial Rounded MT Bold:style=Regular";


//
// Filament swatch card text details, per-line:
//   index, text, font, height, spacing, offset
//
CARD_LINES = [
 [0, "Jaycar PLA",              FONT_ARIAL, 6.4, 1.0, 0.0],
 [1, "Copper filament",         FONT_ARIAL, 4.8, 1.1, 1.8],
 [2, "200\u00B0\u201360\u00B0", FONT_ARIAL, 4.4, 1.1, 2.2]
];

