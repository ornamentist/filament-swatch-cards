//
// OpenSCAD modules to create rounded-edge 3D shapes.
//

//
// Module: create a torus, centered around the Z axis.
//
// Method: rotate (extrude) a circle of `inner` radius around an `outer`
// radius. See the OpenSCAD documentation for more on how this is done.
//
module torus(outer, inner) {
  rotate_extrude()
    translate([outer - inner, 0, 0])
      circle(inner);
}


//
// Module: create a cylinder with rounded top and bottom edges.
//
// Method: place torii just inside the top and bottom faces of the
// cylinder, then take the convex hull of the combined shape.
//
module rounded_cylinder(radius, height, round) {
  hull() {
    translate([0, 0, round])
      torus(radius, 0.1);

    translate([0, 0, height - round])
      torus(radius, 0.1);
  }
}


//
// OpenSCAD doesn't support enums so these named constants represent
// rounded shape choices.
//
PLACE_TORUS    = 1;
PLACE_SPHERE   = 2;
PLACE_CYLINDER = 3;


//
// Module: place a named rounded shape (of two radii) at a location.
// Mainly useful as a helper module for creating convex hull-ed shapes.
//
module place(shape, origin, r1, r2) {
  translate(origin)
    if (shape == PLACE_TORUS) {
      torus(r1, r2);
    } else if (shape == PLACE_SPHERE) {
      sphere(r1);
    } else if (shape == PLACE_CYLINDER) {
      cylinder(r1, r2);
    }
}


//
// Module: create a cuboid with rounded X-Y corners and edges.
//
// Method: place torii at all 8 corners of a cuboid take the convex hull
// of the result. We use the torus radii to round the corners and edges
// differently.
//
module rounded_cuboid(spans, r1, r2) {
  dx = spans.x; dy = spans.y; dz = spans.z;

  hull() {
    place(PLACE_TORUS, [     r1,      r1,      r2], r1, r2);
    place(PLACE_TORUS, [dx - r1,      r1,      r2], r1, r2);
    place(PLACE_TORUS, [dx - r1, dy - r1,      r2], r1, r2);
    place(PLACE_TORUS, [     r1, dy - r1,      r2], r1, r2);
    place(PLACE_TORUS, [     r1,      r1, dz - r2], r1, r2);
    place(PLACE_TORUS, [dx - r1,      r1, dz - r2], r1, r2);
    place(PLACE_TORUS, [dx - r1, dy - r1, dz - r2], r1, r2);
    place(PLACE_TORUS, [     r1, dy - r1, dz - r2], r1, r2);
  }
}


//
// Module: create a cuboid with rounded X-Y corners and top edges.
//
// Method: place cylinders and torii at all 8 corners of a cuboid and
// take the convex hull of the result. We use the torus radii to round
// the corners and top edges differently.
//
module top_rounded_cuboid(spans, r1, r2) {
  dx = spans.x; dy = spans.y; dz = spans.z;

  hull() {
    place(PLACE_CYLINDER, [     r1,      r1,       0], r2, r1);
    place(PLACE_CYLINDER, [dx - r1,      r1,       0], r2, r1);
    place(PLACE_CYLINDER, [dx - r1, dy - r1,       0], r2, r1);
    place(PLACE_CYLINDER, [     r1, dy - r1,       0], r2, r1);
    place(PLACE_TORUS,    [     r1,      r1, dz - r2], r1, r2);
    place(PLACE_TORUS,    [dx - r1,      r1, dz - r2], r1, r2);
    place(PLACE_TORUS,    [dx - r1, dy - r1, dz - r2], r1, r2);
    place(PLACE_TORUS,    [     r1, dy - r1, dz - r2], r1, r2);
  }
}


