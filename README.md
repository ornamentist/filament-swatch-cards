# 3D Printed Filament Swatch Card Box

## Synopsis

This repository contains [OpenSCAD][openscad-URL] (parametric CAD)
libraries for generating 3D printable filament "swatch" cards and a
matching box to hold them. The project was inspired by [these excellent
Fusion 360 card and box models][inspiration-URL].


## Generating 3D printable card box STL's

To generate STL filament swatch boxes, open `filament-box.scad` in
OpenSCAD. This file is usable with the OpenSCAD GUI or the OpenSCAD CLI.

For CLI users, a Gnu `Makefile` is included to generate box STL's. See
below for more on using the Makefile.


## Generating 3D printable filament swatch card STL's.

Users of these OpenSCAD libraries will want to generate swatch cards for
many different filament types. OpenSCAD doesn't support loading code
libraries as a command-line option, so this library collects filament
card-specific text data into the sub-directory `texts`.

The template OpenSCAD file `create-card.template` is a file template
intended to be "expanded" by each filament-specific text OpenSCAD file
in `texts`.

The project Gnu Makefile will perform this process automatically (via
the `cards` sub-directory), or you could create expanded card OpenSCAD
files manually in your text editor.

This approach is admittedly a hack, and will need revisiting.


## Gnu Makefile

Targets provided in the project Gnu `Makefile` include `meshes` to
generate all STL files, and `clean` to remove all regeneratable files.
So far the Makefile has only been configured for, and tested with macOS
12.x.


## Customizing the design parameters

The customizable design parameters for both cards and boxes are
organized in `parameters.scad`. The lines of text engraved in the cards
are organized in the card-specific files in the `texts` sub-directory.

You may need to experiment with different fonts and font settings as
fonts that render well in 2D don't always 3D print legibly, especially
"engraved" in a block of filament.

The card box capacity can range from 1 card up to the limits of your
printer, filament and patience.


## A note on curve fragments

The filament swatch cards can be slow to generate in OpenSCAD, so you
may want to experiment with the OpenSCAD "special variable" `$fn`.
Higher values of `$fn` yield rounder curves, but may take
longer--sometimes a _lot_ longer--to generate.


## Design approach

All the OpenSCAD modules and functions that generate filament swatch
cards are organized in `card-modules.scad` and likewise for card boxes
in `filament-box.scad`. Both of these libraries include
`parameters.scad` and a generic shapes library `rounded-shapes.scad` as
needed.

The only data structure OpenSCAD supports is vectors, so all the
parametric settings are organized into constant vectors. This approach
has the disadvantage of needing magic-number indexes in client code, but
has the advantage of not requiring numerous global variables or long
parameter lists.


## Enhancements

* Add support for multiple notches to represent filament types, e.g. one
  notch for PLA, two notches for PETG, three notches for ABS, etc.

* Add support for validating design parameters--e.g. the box capacity
  (in cards) should be >= 1.

* Generalize the project Makefile to run on operating systems other than
  macOS 12.x.


## Credits

A big thanks to the designer of the card and box models
[here][inspiration-URL] who inspired this OpenSCAD project.


## License

This project is licensed under a [GPLv3 license][license-URL].


<!-- Reference links used above -->

[license-URL]: https://www.gnu.org/licenses/gpl-3.0.html

[openscad-URL]: https://openscad.org

[inspiration-URL]: https://cults3d.com/en/3d-model/various/box-with-filament-samples

