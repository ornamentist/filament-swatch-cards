//
// OpenSCAD modules to create a filament swatch card.
//
include <parameters.scad>
include <rounded-shapes.scad>


//
// Module: a filament swatch card body.
//
// Method: the card body is just a rounded cuboid.
//
module body(card) {
  rounded_cuboid(card[3], card[1], card[2]);
}


//
// Module: a filament swatch card filament type "notch".
//
// Method: the card notch is a rounded edge cylinder. Note that only one
// notch (representing PLA) is currently supported.
//
module notch(notch) {
  translate(notch[3][0])
    rounded_cylinder(notch[0], notch[1], notch[2]);
}


//
// Module: a filament swatch card "inset".
//
// Method: the card inset is just a rounded cuboid.
//
module inset(inset) {
  spans = [inset[3].x, inset[3].y, inset[0]];

  translate(inset[4])
    rounded_cuboid(spans, inset[1], inset[2]);
}


//
// Module: a filament swatch card engraved text block.
//
// Method: each line of card text is sized and positioned manually in
// 2D, then extruded to 3D.
//
module text_block(text, lines) {
  translate(text[1])
    linear_extrude(text[0])
      for (line = lines)
        translate([0.0, -line[0] * line[3] * line[5]])
          text(text = line[1], size = line[3], font = line[2], spacing = line[4]);
}


//
// Module: a complete filament swatch card.
//
// Method: create the card body then difference the notch, inset and
// text block from it.
//
module card(card, inset, notch, text, lines) {
  difference() {
    body(card);
    notch(notch);
    inset(inset);
    text_block(text, lines);
  }
}
