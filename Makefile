#
# Gnu Makefile for generating filament swatch cards and box mesh files
# from OpenSCAD libraries.
#

# Turn off annoying tab-indented rules.
.RECIPEPREFIX = >


# CLI programs (MacOS only so far).
OPENSCAD_CLI := /Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD


# OpenSCAD CLI variable settings.
OPENSCAD_FN := 120
OPENSCAD_FA := 1


#: Print rule for debugging Makefile.
print-%: ; @echo $*=$($*)


# OpenSCAD common source files.
COMMON_FILES := parameters.scad rounded-shapes.scad card-modules.scad


# Make an expanded card OpenSCAD filename
card_SCAD = $(patsubst texts/%.scad,cards/%.scad,$(1))


# Make an expanded card STL filename
card_STL = $(patsubst cards/%.scad,meshes/%.stl,$(1))


# OpenSCAD card box files.
BOX_SCAD_FILES := filament-box.scad
BOX_STL_FILES  := $(patsubst %.scad,meshes/%.stl,$(BOX_SCAD_FILES))


# OpenSCAD card files.
CARD_TEMPLATE_FILE := create-card.template
CARD_EXPAND_FILES  := $(wildcard texts/*.scad)
CARD_SCAD_FILES    := $(foreach f,$(CARD_EXPAND_FILES),$(call card_SCAD,$(f)))
CARD_STL_FILES     := $(foreach f,$(CARD_SCAD_FILES),$(call card_STL,$(f)))


# Don't delete OpenSCAD and mesh files if interrupted.
.SECONDARY: $(CARD_SCAD_FILES)
.PRECIOUS: $(CARD_SCAD_FILES) $(BOX_STL_FILES) $(CARD_STL_FILES)


# Expand an OpenSCAD card template into OpenSCAD code.
cards/%.scad: texts/%.scad $(CARD_TEMPLATE_FILE)
> mkdir -p cards
> cat $^ > $@


# Generate an STL file from an OpenSCAD design.
meshes/%.stl: %.scad $(COMMON_FILES)
> mkdir -p meshes
> $(OPENSCAD_CLI) -o $@ -D '$$fn=$(OPENSCAD_FN)' -D '$$fa=$(OPENSCAD_FA)' $<


# Generate an STL file from an OpenSCAD card expanded design.
meshes/%.stl: cards/%.scad $(COMMON_FILES)
> mkdir -p meshes
> $(OPENSCAD_CLI) -o $@ -D '$$fn=$(OPENSCAD_FN)' -D '$$fa=$(OPENSCAD_FA)' $<


# Make generated mesh files in supported formats.
meshes: $(BOX_STL_FILES) $(CARD_STL_FILES)


# Default rule: generate all mesh files.
.DEFAULT: meshes


# Remove generated files.
.PHONY: clean
clean:
> rm -f meshes/*
> rm -f cards/*.scad

