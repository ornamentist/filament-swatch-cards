//
// OpenSCAD modules to create a filament swatch card box.
//
include <parameters.scad>
include <rounded-shapes.scad>


//
// Helper function: return the card slot spans.
//
function slot_spans(card, box) =
  // Slot spans are based on the card spans.
  let(dx = card[3].x + box[7].x)
  let(dy = card[3].z + box[7].y)
  let(dz = card[3].y * box[6])

  [dx, dy, dz];


//
// Helper function: return the card divider spans.
//
function divider_spans(card, box) =
  // Divider X/Z spans are based on the card spans.
  let(dx = card[3].x + box[7].x)
  let(dz = card[3].y * box[6])

  [dx, box[4], dz];


//
// Helper function: return the i'th card divider origin.
//
function divider_origin(card, box, i) =
  // Y offset from previous card slots.
  let(slots_dy = i * (card[3].z + box[7].y))

  // Y offset from previous card dividers.
  let(dividers_dy = (i - 1) * box[4])

  // Combined divider Y offset.
  let(dy = box[1] + slots_dy + dividers_dy)

  [box[1], dy, box[5]];


//
// Helper function: calculate the box internal spans.
//
function internal_spans(card, box) =
  // Find the spans of a single slot and divider.
  let(slot = slot_spans(card, box))
  let(divider = divider_spans(card, box))

  // Scale the spans by the card capacity.
  let(slots_dy = box[0] * slot.y)
  let(dividers_dy = (box[0] - 1) * divider.y)

  [slot.x, slots_dy + dividers_dy, slot.z];


//
// Module: create a single swatch card box divider.
//
// Method: since rounding 3D edges is difficult and slow in OpenSCAD,
// this module creates the divider profile in 2D, applies rounding in
// 2D, then extrudes and re-orients the divider into 3D.
//
module box_divider(card, box) {
  spans = divider_spans(card, box);

  // Divider relative polygonal coordinates.
  p1 = [spans.x * 0.00, spans.z * 0.0];
  p2 = [spans.x * 0.00, spans.z * 1.0];
  p3 = [spans.x * 0.04, spans.z * 1.0];
  p4 = [spans.x * 0.04, spans.z * 0.6];
  p5 = [spans.x * 0.96, spans.z * 0.6];
  p6 = [spans.x * 0.96, spans.z * 1.0];
  p7 = [spans.x * 1.00, spans.z * 1.0];
  p8 = [spans.x * 1.00, spans.z * 0.0];

  // Create the 2D divider polygon, offset sharp curves then re-orient
  // into 3D.
  translate([0, box[4], 0])
    rotate(90, X_AXIS)
      linear_extrude(box[4])
        offset(-5.0)
          offset(5.0)
            polygon([p1, p2, p3, p4, p5, p6, p7, p8]);
}


//
// Module: create a complete swatch card box.
//
// Method: create "external" and "internal" card boxes and difference
// them to create an empty box shell. Then add individual card dividers
// into the box.
//
module card_box(card, box) {
  spans = internal_spans(card, box);

  // Allow for the internal spans and box walls.
  dx = spans.x + (2 * box[1]);
  dy = spans.y + (2 * box[1]);
  dz = spans.z + box[5];

  // Remove the internal box from the box spans.
  difference() {
    top_rounded_cuboid([dx, dy, dz], box[2], box[3]);

    translate([box[1], box[1], box[5]])
      cube(spans);
  }

  // Place each card divider (one less dividers than slots).
  for (i = [1:box[0] - 1]) {
    translate(divider_origin(card, box, i))
      box_divider(card, box);
  }
}


// Generate a complete filament swatch card box from the design
// parameters.
card_box(CARD_BODY, BOX_BODY);
